import java.util.Scanner;


public class SimpleWar {
    public static void main(String[] args) {
        java.util.Scanner reader = new java.util.Scanner(System.in);
		
		Deck deck = new Deck();
		deck.shuffle();
		
		System.out.println(deck.toString());
		
		int points1 = 0;
		int points2 = 0;
		
		while (deck.length() > 1) {
			
			Card card1 = deck.drawTopCard();
			Card card2 = deck.drawTopCard();
			
			System.out.println("PLAYER 1 CARD : \n" + card1.toString());
			System.out.println(card1.calculateScore());
			System.out.println("PLAYER 2 CARD : \n" + card2.toString());
			System.out.println(card2.calculateScore());
			
			if (card1.calculateScore() > card2.calculateScore()) {
				System.out.println("player1 wins");
				points1++;
			}	else {
				System.out.println("player2 wins");
				points2++;
			}
		
			System.out.println("Player 1: " + points1);
			System.out.println("Player 2: " + points2);
			
			System.out.println("________________________________________");
		}
		
		if (points1 > points2){
			System.out.println("VICTORY TO PLAYER 1");
		}	else if (points2 > points1){
			System.out.println("VICTORY TO PLAYER 2");
		}	else {
			System.out.println("HOW DID THIS EVEN HAPPEN");
		}
    


	}
	
	
}