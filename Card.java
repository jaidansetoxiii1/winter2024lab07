public class Card {

	//fields
	private String suit;
	private int value;
	
	//Constructor Method
	public Card (String iSuit, int iValue){
		this.suit = iSuit;
		this.value = iValue;
	}
	
	//Getter Methods
	public String getSuit(){
		return this.suit;
	}
	
	public int getValue(){
		return this.value;
	}
	
	//Setter Methods
	public void setSuit(String newSuit){
		this.suit = newSuit;
	}
	
	public void setValue(int newValue){
		this.value = newValue;
	}
	
	//toString Method
	public String toString() {
		if (this.value == 1){
			return ("Ace of " + this.suit + "s");
			
		} else if (this.value == 11){
			return ("Jack of " + this.suit + "s");
			
		}	else if (this.value == 12){
			return ("Queen of " + this.suit + "s");
			
		}	else if (this.value == 13){
			return ("King of " + this.suit + "s");
			
		} else {
			return (this.value + " of " + this.suit + "s");
		}
	}
	
	//Check Score of Card
	public double calculateScore() {
		double score = this.value;
		
		if (this.suit.equals("Heart")) {
			score += 0.4;
		}	if (this.suit.equals("Diamond")) {
			score += 0.3;
		}	if (this.suit.equals("Club")) {
			score += 0.2;
		}	if (this.suit.equals("Spade")) {
			score += 0.1;
		}
		
		return score;
	}
}