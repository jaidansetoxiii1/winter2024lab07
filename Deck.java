import java.util.Scanner;
import java.util.Random;

//The Deck class is where AN ENTIRE playing card deck is created.

public class Deck {
	private Card[] cards;
	private int numberOfCards;
	private Random rng;

	public Deck(){
		//Fields
		this.numberOfCards = 52;
		this.rng = new Random();
		this.cards = new Card[numberOfCards];
		String suits[] = new String[]{"Spade","Diamond","Club","Heart"};

		//creates each card in the deck one at a time.
		for (int i = 0; i < cards.length; i++){
			cards[i] = new Card(suits[rng.nextInt(4)], rng.nextInt(1, 13));
		}


	}

	//Getter for the current deck size.
	public int length(){
		return this.numberOfCards;
	}

	//Drawing cards doesn't change the array size, but it reduces the index
	// for other methods.
	public Card drawTopCard()	{
		Card cardDrawn = cards[this.numberOfCards - 1];
		this.numberOfCards -= 1;

		return cardDrawn;
	}

	//Returns a handy String that tells you which position each card is in and its
	//stats.
	public String toString()	{
		String deck = "";
		for (int i = 0; i < this.numberOfCards; i++){
			deck = deck + (i + 1) + ". " + cards[i].toString() + ",\n";
		}

		return deck;
	}

	//Shuffles the deck by swapping random cards with eachother.
	public void shuffle(){
		for (int i = 0; i < this.numberOfCards - 1; i++) {
			Card tempCard = cards[i];
			int randPosition = rng.nextInt(i, this.numberOfCards);

			this.cards[i] = this.cards[randPosition];
			this.cards[randPosition] = tempCard;
		}
	}
}